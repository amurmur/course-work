import React, { Component } from 'react';
import Details from '../components/Details/Details';
import { BrowserRouter as Router, Route, Switch,  withRouter } from 'react-router-dom';

class ForecastPage extends Component{

  constructor(props) {
    super(props);

    this.state = {
      city: this.props.city,
      forecasts: this.props.forecasts
    };

    console.log(this.state.city);

}

  render() {
    return (
      <div>
        <Details
          city={this.state.city}
          forecasts={this.state.forecasts} />
      </div>
    )
  }
};

export default withRouter(ForecastPage);