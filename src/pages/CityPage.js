import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch,  withRouter } from 'react-router-dom';
import Forecast from '../components/Forecast/Forecast';
var helpers = require('../helpers/fetchWeather');

class CityPage extends Component{
 
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      city: '',
      forecasts: []
    };

    this.handleClick = this.handleClick.bind(this);
  }


  componentDidMount() {
    this.retrieveForecast(this.props.match.params.city);
  }

  retrieveForecast(city) {
    helpers.get5DayForecast(city)
      .then(function(data) {
        this.setState({
          loading: false,
          city: data.city.name,
          forecasts: data.list
        });
      }.bind(this));
  }

  handleClick(forecast) {
   this.props.history.push({
      pathname: '/detail/' + this.state.city
    });
    this.props.updateCity(this.state.city);
    this.props.updateForecasts(forecast);
  }

  render() {
    return (
      <div>
        <Forecast
          loading={this.state.loading}
          city={this.state.city}
          forecasts={this.state.forecasts}
          handleClick={this.handleClick}/>
      </div>
    )
  }
};

export default withRouter(CityPage);