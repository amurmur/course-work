import React, { Component } from 'react';
import GetCity from '../components/GetCity/GetCity';
import MapView from '../components/Map/MapView';
import Geo from '../components/Geo/Geo';
import { Route , withRouter} from 'react-router-dom';
import useLocalStorage from 'react-use-localstorage';

class HomePage extends Component {

  constructor(props) {
    super(props);

    this.state = {
        city: localStorage.getItem('city'),
        lat:'',
        lng:''
    };

    this.updateInput = this.updateInput.bind(this);
    this.submitForm = this.submitForm.bind(this);

}


  updateInput(e) {
    /*this.setState({ city: e.target.value });*/

    localStorage.setItem('city', e.target.value);
    this.setState({city: e.target.value});
  }

  submitForm(e) {
    e.preventDefault();
    this.props.history.push('/forecast/' + this.state.city)
    this.setState({ city: '' });
  }

  updateLat= (value) => {
      this.setState({ lat: value })
  };

  updateLng = (value) => {
      this.setState({ lng : value })
  };

  render() {
    return (
      <div>
        <GetCity
          city={this.state.city}
          onUpdateInput={this.updateInput}
          handleSubmit={this.submitForm}
          />
        <Geo lat={this.state.lat} lng={this.state.lng}/>
        <MapView updateLat={this.updateLat} updateLng={this.updateLng}/>
      </div> 
    )
  }
};

export default withRouter(HomePage);