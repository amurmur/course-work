import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from './components/Header/Header';

import HomePage from './pages/HomePage';
import CityPage from './pages/CityPage';
import ForecastPage from './pages/ForecastPage';

import styles from './App.css';


class App extends Component {
	constructor() {
    super();

    this.state = {
      city: '',
      forecasts: [],
    };

    this.updateForecasts = this.updateForecasts.bind(this);
    this.updateCity = this.updateCity.bind(this);
}

    updateCity = (value) => {
    	this.setState({ city: value })
	};

	updateForecasts = (value) => {
    	this.setState({ forecasts : value })
	};


	render() {
		return (
			<Router>
				<Header/>
				<Switch>
			      <Route exact path='/' component={HomePage}/>
			      <Route path='/forecast/:city' render={()=><CityPage updateCity={this.updateCity} updateForecasts={this.updateForecasts}/>} />
      			  <Route path='/detail/:city'  render={()=><ForecastPage city={this.state.city} forecasts={this.state.forecasts}/>} /> 
			    </Switch>
			</Router>
		);
	}
}

export default App;
