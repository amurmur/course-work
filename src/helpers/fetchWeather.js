var fetchWeathers = {
  getCurrentWeather(city) {
    return fetch('http://api.openweathermap.org/data/2.5/weather?q='+city+'&type=accurate&APPID=0fe4e2b61b814cc0158ee3c6ab1a14c9')
    .then(response => response.json())
    .then(data => data);
      },

  get5DayForecast(city) {
    return fetch('http://api.openweathermap.org/data/2.5/forecast/daily?q='+city+'&type=accurate&APPID=0fe4e2b61b814cc0158ee3c6ab1a14c9&cnt=5')
    .then(response => response.json())
    .then(data => data)
  }
}

module.exports = fetchWeathers;