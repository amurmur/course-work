import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

function getLocation(props) {
    const google = window.google;
    var lat = props.lat;
    var lng = props.lng;
    var geocoder;
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);

    geocoder.geocode(
        {'latLng': latlng}, 
        function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var add= results[0].formatted_address ;
                    var  value=add.split(",");
                    const count=value.length;
                    const country=value[count-1];
                    const state=value[count-2];
                    const city=value[count-3];
                    return <div>{city},{country}</div>;;
                }
                else  {
                   return <div> address not found </div>;
                }
            }
            else {
                return <div> Geocoder failed due to: {status} </div>;
            }
        }
    );
}

export default getLocation;
