import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Link } from 'react-router-dom'

import styles from './Header.module.css';

export default (props) => { 
  return (
    <div className={styles.header}>
      <Link to='/' className={styles.link}>
        <h1 className={styles.h1}>WEATHER APP</h1>
      </Link>
    </div>
  )
}
