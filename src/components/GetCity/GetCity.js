import React from 'react';
import PropTypes from 'prop-types';

import styles from './GetCity.module.css';


class GetCity extends React.Component {
  render() {
  return (
  <div className={styles.getcity}>
    <label className={styles.label} for="cityname">Enter a place name below</label>
    <form className={styles.form} onSubmit={this.props.handleSubmit}>
      <input
        type='text'
        value={this.props.city}
        className='form-control'
        placeholder='Enter a town or city name'
        onChange={this.props.onUpdateInput}/>
      <button
         className={styles.button}>
        Search
      </button>
    </form>
  </div>
  )
}
}

GetCity.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onUpdateInput: PropTypes.func.isRequired
};


export default GetCity;