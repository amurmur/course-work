import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import dateHelpers from '../../helpers/dateHelpers';
import styles from './Forecast.css';


function ForecastList(props) {
    var forecasts = props.forecasts.map(function(data, i) {

    var imgSrc = 'http://openweathermap.org/img/w/'+data.weather[0].icon+'.png';
    var date = new Date(data.dt * 1000);

    return (
      <div id='forecast' key={i} style={styles.forecast} onClick={props.handleClick.bind(null,data)}>
        <p>{data.weather[0].description}</p>
        <p>{dateHelpers(date)}</p>
        <img src={imgSrc} alt='Weather Icon'/>
      </div>
    );
  });

  return <div>{forecasts}</div>;
}

function Forecast(props) {
  if (props.loading === true) {
    return <h3 id='loading' style={styles.loading}>Loading...</h3>
  } else {
    return (
      <div id='forecastContainer'>
        <h2>{props.city}</h2>
        <p>Pick a day</p>
        <ForecastList 
          forecasts={props.forecasts}
          handleClick={props.handleClick}
          />
        <hr />
        <Link to='/'>
          <button style={styles.button} id='button'>Home</button>
        </Link>
      </div>
    );
  }
}

export default Forecast;