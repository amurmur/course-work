import React, {Component} from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Link } from 'react-router-dom'
import Geocode from 'react-geocode';

import styles from './Geo.css';

const API_KEY = 'AIzaSyALrSTvIhL5OThfvK5diki3wdP10UuplyU'

class Geo extends Component {

  constructor(props){
    super(props)
    this.state = {
        city: ''
    }
  }


  componentDidMount() {
    if (this.props.lat & this.props.lng) {
    this.getLocation(this.props.lat, this.props.lng)}
  }

  componentDidUpdate() {
    if (this.props.lat & this.props.lng) {
    this.getLocation(this.props.lat, this.props.lng)}
  }

  getLocation = (lat,lng) => {
    Geocode.setApiKey("AIzaSyALrSTvIhL5OThfvK5diki3wdP10UuplyU");
    Geocode.enableDebug();
    Geocode.fromLatLng(lat,lng).then(
      response => {
        const address = response.results[0].formatted_address;
        const adr=address.split(', ');
        const myCity=adr[adr.length-4];
        this.setState ({city: myCity})
      },
      error => {
        console.error(error);
      })
  }


render() {
    return (
    <div  id='geo'>
       <h2>OR</h2>
       <p id='p1' >Use automatically determined location</p>
       <p id='p2'>Are you interested in the weather forecast in {this.state.city}</p>
       <Link to={`/forecast/${this.state.city}`} className={styles.link}>
       <button id='button'>
          Yes
        </button>
        </Link>
    </div>
    )
}
}

export default Geo;