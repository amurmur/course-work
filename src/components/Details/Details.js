import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom';
import dateHelpers from '../../helpers/dateHelpers'
import styles from './Details.css'


function Details(props) {
  var forecasts = props.forecasts;
  var date = new Date(forecasts.dt * 1000);
  var imgSrc = 'http://openweathermap.org/img/w/'+forecasts.weather[0].icon+'.png';
  

  return (
    <div id='detailsContainer'>
      <h3>{props.city}</h3>
      <p>{dateHelpers(date)}</p>
      <img src={imgSrc} alt='Weather Icon'/>
      <p>{forecasts.weather[0].description.charAt(0).toUpperCase() + forecasts.weather[0].description.slice(1)}</p>
      <p>Max: {parseInt(forecasts.temp.max - 273.15)} ºC</p>
      <p>Min: {parseInt(forecasts.temp.min - 273.15)} ºC</p>
      <p>Humidity: {forecasts.humidity} %</p>
      <hr />
      <Link to='/'>
        <button id='buttonHome'>Home</button>
      </Link>
    </div>
  );
}

export default Details;